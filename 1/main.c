// 2015110705 김민규
// 본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#define MAX_SIZE 24
#define FALSE 0
#define TRUE 1

typedef struct node *nodePointer;
typedef struct node {
	int data;
	nodePointer link;
	}node;

int main()
{
	short int out[MAX_SIZE];
	nodePointer seq[MAX_SIZE];
	nodePointer x, y, top;
	int i, j, n;

	//파일 입출력
	FILE* pInputFile = fopen("input.txt", "r");

	printf("/* MAX_SIZE of a set S : 24 */ \n");

	//사이즈 입력
	fscanf(pInputFile, "%d", &n);
	printf("current size of S : %d \n", n);

	//집합 입력
	for (i = 0; i < n; i++) {
		/* initialize seq and out */
		out[i] = FALSE;	seq[i] = NULL;
	}

	printf("S = < 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 > \n");

	printf("input pairs : ");
	/* Phase 1 : Input the equivalence pairs : */

	while (!feof(pInputFile)) {
		fscanf(pInputFile, "%d %d ",&i,&j);
		printf("%dR%d ",i, j);
		x = (nodePointer)malloc(sizeof(node));
		x->data = j; x->link = seq[i]; seq[i] = x;
		x = (nodePointer)malloc(sizeof(node));
		x->data = i; x->link = seq[j]; seq[j] = x;
	}
	printf("\n");
	fclose(pInputFile);

	/* Phase 2 : output the equivalence classes */
	for (i = 0; i < n; i++) {
		if(out[i] == FALSE) {
			printf("\nNew class : %5d", i);
			out[i] = TRUE;		/* set class to true */
			x = seq[i]; top = NULL; /* initialize stack */
			while(1) {			/* find rest of class */
				while (x) {		/* process list */
					j = x->data;
					if(out[j] == FALSE) {
						printf("%5d", j);		out[j] = TRUE;
						y = x->link; x->link = top; top = x; x = y;
					}
					else x = x->link;
				}
				if(!top) break;
				x = seq[top->data]; top = top->link;
													/* unstack */
			}	
		}
	}
	printf("\n");

	return 0;
}