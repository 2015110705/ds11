// 2015110705 김민규
// 본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>

typedef struct node *nodePointer;
typedef struct node {
	nodePointer prevNode;
	int data;
	nodePointer nextNode;
	} node;
nodePointer header = NULL;

void dinsert(nodePointer node, nodePointer newNode);
void ddelete(nodePointer node, nodePointer deleted);
void printBackward(const nodePointer node);
void printForward(const nodePointer node);
void dataInput(nodePointer *head);
void addNewNode(int data, nodePointer *head);

int main()
{
	nodePointer head = NULL;
	nodePointer temp;

	dataInput(&head);
	printf("After creating a doubly linkned circular list with a head node : \n");
	printForward(head);
	printBackward(head);

	printf("\n After deleteing numbers less than and equal to 50 : \n");
	temp = head;
	for (temp = head->nextNode; temp != head;) {
		if(temp->data <= 50) {
			temp = temp->nextNode;
			ddelete(head, temp->prevNode);
		}
		else temp = temp->nextNode;
	}
	printForward(head);
	printBackward(head);

	printf("\nAfter deleteing all nodes except for the header node : \n");
	for(temp = head->prevNode; temp != head;) {
		temp = temp->prevNode;
		ddelete(head, temp->nextNode);
	} 
	printForward(head);
	printBackward(head);

	return 0;
}

void dinsert(nodePointer node, nodePointer newNode)
{
	/* insert newnode to the right of node */
	newNode->prevNode = node;
	newNode->nextNode = node->nextNode;
	node->nextNode->prevNode = newNode;
	node->nextNode = newNode;
}

void ddelete(nodePointer node, nodePointer deleted)
{
	/* delete from the doubly linked list */
	if (node == deleted) {
		printf("Deletion of header node not permitted. \n");
		exit(0);
	}
	else {
		deleted->prevNode->nextNode = deleted->nextNode;
		deleted->nextNode->prevNode = deleted->prevNode;
		free(deleted);
	}
}

void printBackward(const nodePointer node)
{
	nodePointer temp;
	int escape = 1;

	printf("backward \n");
	for (temp = node->prevNode; temp != node; temp = temp->prevNode) {
		printf("%2d ", temp->data);
		if(escape % 10 == 0) {
			printf("\n");
		}
		escape++;
	}
	printf("\n");
}

void printForward(const nodePointer node)
{
	nodePointer temp;
	int escape = 1;
	printf("forward \n");
	for (temp = node->nextNode; temp != node; temp = temp->nextNode) {
		printf("%2d ", temp->data);
		if(escape % 10 == 0) {
			printf("\n");
		}
		escape++;
	}
	printf("\n");
}

void dataInput(nodePointer *head)
{
	FILE* pInputFile = fopen("input.txt", "r");
	int buffer;

	addNewNode(0, head);
	while(!feof(pInputFile)) {
		fscanf(pInputFile, "%d ", &buffer);
		addNewNode(buffer, head);
	}

	fclose(pInputFile);
}

void addNewNode(int data, nodePointer *head)
{
	nodePointer newNode;

	newNode = (nodePointer)malloc(sizeof(node));
	
	if(*head == NULL) {
		newNode->nextNode = newNode;
		newNode->prevNode = newNode;
		*head = newNode;
	}
	else {
		newNode->data = data;
		newNode->nextNode = NULL;
		newNode->prevNode = NULL;
		dinsert((*head)->prevNode, newNode);
	}
}